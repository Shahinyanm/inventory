export interface IInventoryItemInterface {
    id: string;
    type?: InventoryItemType,
    name?: string;
    imageUrl?: string;
    count?: number;
    charges?: number;
    maxCharges?: number;
    cooldown?: number;
    isEmpty?: boolean;
}

export type InventoryItemType = 'misc' | 'armor' | 'weapon'