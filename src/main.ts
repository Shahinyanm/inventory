import {createApp} from 'vue'
import PerfectScrollbar from "vue3-perfect-scrollbar";
import FloatingVue from 'floating-vue'

import App from './App.vue'


import 'floating-vue/dist/style.css'
import 'vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css'
import './assets/styles/main.scss'

const app = createApp(App)

app.use(FloatingVue, {
    theme: {
        'info-tooltip': {
            $extend: 'tooltip',
        },
        'other-tooltip': {
            $extend: 'info-tooltip',
        }
    }
});
app.use(PerfectScrollbar);

app.mount('#app');
