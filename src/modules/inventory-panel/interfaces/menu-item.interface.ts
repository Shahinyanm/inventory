import type { MenuItemLabelsType } from '@/modules/inventory-panel/types/menu-item.types'
import {MenuItems} from '@/modules/inventory-panel/types/menu-item.types';

export interface IActiveMenuItem {
    label: MenuItemLabelsType,
    value: MenuItems
}