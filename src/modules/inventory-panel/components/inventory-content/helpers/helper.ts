import type {IInventoryItemInterface} from "@/shared/interfaces/inventory/inventory-item.interface";
import {generateUUID} from "@/shared/helpers/common.helper";
import type {MenuItems} from "@/modules/inventory-panel/types/menu-item.types";
import {MenuItems} from "@/modules/inventory-panel/types/menu-item.types";

export function getCaseNumber(): number {
    const queryParams = new URLSearchParams(location.search)
    const caseParamValue = parseInt(queryParams.get('case') as string, 10);

    return (caseParamValue && caseParamValue <= 3) ? caseParamValue : 1;
}


const MIN_INVENTORY_ELEMENTS_COUNT = 40;
export function getNormalizedData(data: IInventoryItemInterface[]): INormalizedInventoryData {
    const missingElementsCount = MIN_INVENTORY_ELEMENTS_COUNT - data.length;
    let inventoryItemsArr = data;
    if (missingElementsCount > 0) {
        inventoryItemsArr = [...data];
        for (let i = 0; i < missingElementsCount; i++) {
            inventoryItemsArr.push({
                id: generateUUID(),
                isEmpty: true
            })
        }
    }
    return {
        data: inventoryItemsArr,
        maxCooldown: Math.max(...data.map(item => item.cooldown || null))
    };
}

export function getFilteredData(data: IInventoryItemInterface[], type: MenuItems): INormalizedInventoryData {
    if (type === MenuItems.ALL_ITEMS) {
        return getNormalizedData(data);
    }
    const filteredData =  data.filter(item => {
        return item.type === type.toLowerCase();
    });
    return getNormalizedData(filteredData);
}

export interface INormalizedInventoryData {
    data: IInventoryItemInterface[],
    maxCooldown: number | null;
}