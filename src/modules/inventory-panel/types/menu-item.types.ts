export enum MenuItems {
    ALL_ITEMS = 'ALL_ITEMS',
    ARMOR = 'ARMOR',
    WEAPON = 'WEAPON',
    MISC = 'MISC'
}

export const MenuItemLabels = {
    [MenuItems.ALL_ITEMS]: 'All Items',
    [MenuItems.ARMOR]: 'Armor',
    [MenuItems.WEAPON]: 'Weapon',
    [MenuItems.MISC]: 'Misc'
}

export type MenuItemLabelsType = typeof MenuItemLabels[keyof typeof MenuItemLabels];